@extends('layouts.app')

@section('content')

    @if($item->exists)
        <form method="POST" action="{{route('blog.posts.update', $item->id)}}">
            @method('PATCH')
            @else
                <form method="POST" action="{{route('blog.posts.store')}}">
                    @endif
                    @csrf
                    <div class="container">
                        @include('blog.includes.result_messages')
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                @include('blog.posts.includes.edit_main_bloc')
                            </div>
                            <div class="col-md-3">
                                @include('blog.posts.includes.edit_add_bloc')
                            </div>
                        </div>
                    </div>
                </form>

                @if($item->exists)
                    <div class="container">
                        <form method="POST" action="{{route('blog.posts.destroy', $item->id)}}">
                            @method('DELETE')
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="card card-block">
                                        <div class="card-body ml-auto">
                                            <button type="submit" class="btn btn-danger">
                                                Видалити
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
@endsection
