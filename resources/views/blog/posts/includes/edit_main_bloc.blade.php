<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="active nav-link" data-toggle="tab" href="#maindata" role="tab">Основні дані</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane active" id="maindata" role="tabpanel">
                        <div class="form-group">
                            <label for="title">Заголовок</label>
                            <input name="title" value="{{old('title', $item->title)}}"
                                   id="title" class="form-control"
                                   type="text" minlength="3" required>
                        </div>

                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input name="slug" value="{{$item->slug}}"
                                   id="slug" class="form-control"
                                   type="text">
                        </div>
                        <div class="form-group">
                            <label for="category_id">Категорія</label>
                            <select name="category_id" id="category_id" class="form-control"
                                    placeholder="Виберіть категорію" required>
                                @foreach($listCategory as $categoryOption)
                                    <option value="{{$categoryOption->id}}"
                                        @if($categoryOption->id == $item->category_id) selected  @endif>
                                        {{$categoryOption->id_title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="description">Опис</label>
                            <textarea name="description" id="description" class="form-control" rows="20">
                                    {{old('description', $item->description)}}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
