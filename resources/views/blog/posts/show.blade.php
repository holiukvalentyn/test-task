@extends('layouts.app')

@section('content')

    @if($item->exists)

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    {{--                    @include('blog.posts.includes.edit_main_bloc')--}}
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title"></div>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="active nav-link" data-toggle="tab" href="#maindata" role="tab">Основні
                                                дані</a>
                                        </li>
                                    </ul>
                                    <br>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="maindata" role="tabpanel">
                                            <div class="form-group">
                                                <label for="title">Заголовок</label>
                                                <p id="title" class="form-control">{{$item->title}}</p>
                                            </div>

                                            <div class="form-group">
                                                <label for="slug">Slug</label>
                                                <p id="slug" class="form-control">{{$item->slug}}</p>
                                            </div>

                                            <div class="form-group">
                                                <label for="category_id">Категорія</label>
                                                <p id="category_id" class="form-control">{{$item->category->title}}</p>

                                            </div>

                                            <div class="form-group">
                                                <label for="description">Опис</label>
                                                <p id="description" class="form-control">{{$item->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="POST" action="{{route('blog.posts.edit', $item->id)}}">
                                        @method('GET')
                                        @csrf
                                        <button type="submit" class="btn btn-outline-primary">
                                            Редагувати
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="POST" action="{{route('blog.posts.destroy', $item->id)}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">
                                            Видалити
                                        </button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="list-unstyled">
                                        <li>ID: {{$item->id}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Створено</label>
                                        <input value="{{$item->created_at}}" class="form-control" type="text"
                                               disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Змінено</label>
                                        <input value="{{$item->updated_at}}" class="form-control" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

    @endif
@endsection
