<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="active nav-link" data-toggle="tab" href="#maindata" role="tab">Основні дані</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane active" id="maindata" role="tabpanel">
                        <div class="form-group">
                            <label for="title">Заголовок</label>
                            <input name="title" value="{{old('title', $item->title)}}"
                                   id="title" class="form-control"
                                   type="text" minlength="3" required>
                        </div>

                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input name="slug" value="{{$item->slug}}"
                                   id="slug" class="form-control"
                                   type="text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
