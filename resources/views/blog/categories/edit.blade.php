@extends('layouts.app')

@section('content')

    @if($item->exists)
        <form method="POST" action="{{route('blog.categories.update', $item->id)}}">
           @method('PATCH')
    @else
         <form method="POST" action="{{route('blog.categories.store')}}">
    @endif
           @csrf
                    <div class="container">
                        @include('blog.includes.result_messages')
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                @include('blog.categories.includes.edit_main_block')
                            </div>
                            <div class="col-md-3">
                                @include('blog.categories.includes.edit_add_block')
                            </div>
                        </div>
                    </div>
                </form>
@endsection
