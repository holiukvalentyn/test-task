@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-primary" href="{{route('blog.categories.create')}}">Додати категорію</a>
                </nav>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Категорії</th>

                            </tr>
                            </thead>
                            @foreach($paginator as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>
                                        <a href="{{route('blog.categories.edit', $item->id)}}">
                                            {{$item->title}}</a>
                                    </td>

{{--                                    <td>{{count($item->post->id)}}</td>--}}
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{$paginator->links()}}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
