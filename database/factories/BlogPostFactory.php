<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\BlogPost;
use Illuminate\Support\Str;

class BlogPostFactory extends Factory
{

    protected $model = BlogPost::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(3, 8),true);

        $data = [
                'category_id' => rand(1, 11),
                'title' => $title ,
                'slug' => Str::slug($title),
                'description' => $this->faker->realText(rand(1000, 4000)),
//                'content_raw' => $this->faker->realText(rand(800, 3000)),
//                'content_html' => $this->faker->realText(rand(800, 3000)),
        ];
        return $data;
    }
}
