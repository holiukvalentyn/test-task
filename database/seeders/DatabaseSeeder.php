<?php

namespace Database\Seeders;

use App\Models\BlogPost;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(BlogCategoriesTableSeeder::class);
        BlogPost::factory()->count(30)->create();
//        factory(BlogPost::class, 100)->create();
    }
}
