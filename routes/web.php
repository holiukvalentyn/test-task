<?php

use App\Http\Controllers\Blog\CategoryController;
use App\Http\Controllers\Blog\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('/rest', 'App\Http\Controllers\RestTestController')->names('restTest');
//Route::resource('posts', 'App\Http\Controllers\Blog\PostController')->names('blog.posts');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route::group(['prefix' => 'blog'], function (){
//    Route::resource('posts', PostController::class)->names('blog.posts');
//});

Route::group(['prefix' => 'blog'], function () {
    //BlogCategory
    $methods = ['index', 'edit', 'store', 'create', 'update'];
    Route::resource('categories', CategoryController::class)->only($methods)->names('blog.categories');

    //BlogPost
    Route::resource('posts', PostController::class)->names('blog.posts');

});



