<?php

namespace App\Observers;

use App\Models\BlogPost;
use Illuminate\Support\Str;

class BlogPostObserver
{
    /**
     * @param BlogPost $blogPost
     *
     * @return void
     */
    public function creating(BlogPost $blogPost)
    {
        $this->setSlug($blogPost);
    }

    public function updating(BlogPost $blogPost)
    {
//        $test[] = $blogPost->isDirty();
//dd($test);

        $this->setSlug($blogPost);
    }

    /**
     * @param BlogPost $blogPost
     *
     * @return void
     */
    protected function setSlug(BlogPost $blogPost)
    {

        if (empty($blogPost->slug)) {
            $blogPost->slug = Str::slug($blogPost->title);
        }
    }

    /**
     * Handle the BlogPost "created" event.
     *
     * @param \App\Models\BlogPost $blogPost
     *
     * @return void
     */
    public function created(BlogPost $blogPost)
    {
        //
    }

    /**
     * Handle the BlogPost "updated" event.
     *
     * @param \App\Models\BlogPost $blogPost
     *
     * @return void
     */
    public function updated(BlogPost $blogPost)
    {
        $this->setSlug($blogPost);
    }

    public function deleting(BlogPost $blogPost)
    {
//        dd(__METHOD__, $blogPost);
    }

    /**
     * Handle the BlogPost "deleted" event.
     *
     * @param \App\Models\BlogPost $blogPost
     *
     * @return void
     */
    public function deleted(BlogPost $blogPost)
    {
        dd(__METHOD__, $blogPost);
    }

    /**
     * Handle the BlogPost "restored" event.
     *
     * @param \App\Models\BlogPost $blogPost
     *
     * @return void
     */
    public function restored(BlogPost $blogPost)
    {
        //
    }

    /**
     * Handle the BlogPost "force deleted" event.
     *
     * @param \App\Models\BlogPost $blogPost
     *
     * @return void
     */
    public function forceDeleted(BlogPost $blogPost)
    {
        //
    }

}
