<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

abstract class BaseController extends Controller
{
    //для загальних моментів для дочірніх класів
    /**
     *
     */
    public function __construct()
    {
    }
}
