<?php

namespace App\Http\Controllers\Blog;

use App\Http\Requests\Request\BlogPostCreateRequest;
use App\Http\Requests\Request\BlogPostUpdateRequest;
use App\Models\BlogPost;
use App\Repositories\BlogCategoryRepository;
use App\Repositories\BlogPostRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends BaseController
{

    private $blogPostRepository;

    /**
     * @var BlogCategoryRepository|BlogCategoryRepository&Application|Application|\Illuminate\Foundation\Application|mixed
     */
    private $blogCategoryRepository;

    public function __construct()
    {
        parent::__construct();
        $this->blogPostRepository = app(BlogPostRepository::class);
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $paginator = $this->blogPostRepository->getAllWithPaginate(10);

        return view('blog.posts.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $item = new BlogPost();
        $listCategory = $this->blogCategoryRepository->getForcomboBox();

        return view('blog.posts.edit', compact('item', 'listCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(BlogPostCreateRequest $request)
    {
        $data = $request->input();
        $item = (new BlogPost())->create($data);

        if ($item) {
            return redirect()->route('blog.posts.edit', [$item->id])->with(['success' => 'Збережено успішно']);
        } else {
            return back()->withErrors(['msg' => 'Помилка збереження'])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     */
    public function show($id)
    {
        $item = $this->blogPostRepository->getShow($id);
        return view('blog.posts.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     */
    public function edit($id)
    {
        $item = $this->blogPostRepository->getEdit($id);
        if (empty($item)) {
            abort(404);
        }

        $listCategory = $this->blogCategoryRepository->getForcomboBox();

        return view('blog.posts.edit', compact('item', 'listCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */

    public function update(BlogPostUpdateRequest $request, $id)
    {
        $item = $this->blogPostRepository->getEdit($id);

        if (empty($item)) {
            return back()
                ->withErrors(['msg' => "Запис id=[{$id}] не знайдено"])
                ->withInput();
        }

        $data = $request->all();
        $result = $item->update($data);

        if ($result) {
            return redirect()->route('blog.posts.edit', $item->id)->with(['success' => 'Збережено успішно']);
        } else {
            return back()->withErrors(['msg' => "Запис не збережено"])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $result = BlogPost::destroy($id);
        $result = BlogPost::find($id)->forceDelete();

        if ($result){
            return redirect()->route('blog.posts.index')->with(['success' => "Запис id[$id] видалено"]);
        }else{
            return back()->withErrors(['msg' => 'Помилка видалення']);
        }
    }
}
