<?php

namespace App\Http\Requests\Request;

use Illuminate\Foundation\Http\FormRequest;

class BlogPostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:200|unique:blog_posts',
            'slug' => 'max:200|unique:blog_posts',
            'description' => 'required|min:5|max:10000',
            'category_id' => 'required|integer|exists:blog_categories,id',
        ];
    }
    public function messages(){
        return [
            'title.unique' => 'Введіть іншу назву',
            'title.required' => 'Введіть назву статті',
            'description.min' => 'Мінімальна довжина опису [:min] символів',
            'slug.unique' => 'Введіть інший slug',
        ];
    }
    public function attributes(){
        return [
            'title' => 'Заголовок',
        ];
    }
}
