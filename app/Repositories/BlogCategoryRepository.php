<?php

namespace App\Repositories;

use App\Models\BlogCategory as Model;
use Illuminate\Database\Eloquent\Collection;

class BlogCategoryRepository extends CoreRepositories
{
    /**
     * @return string
     */
    protected function GetModelClass()
    {
        return Model::class;
    }

    /**
     * @param $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }


    //для списку категорій в постах
    public function getForcomboBox()
    {
//        return $this->startConditions()->all();
        $columns = implode(', ', [
            'id', 'CONCAT (id, ". ", title) AS id_title '
        ]);

        $result = $this->startConditions()
            ->selectRaw($columns)
            ->toBase()
            ->get();
//        dd($result)

        return $result;
    }


    /**
     * @param $perPage
     *
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = ['id', 'title'];
        $result = $this->startConditions()->select($columns)
            ->with(['post'])
            ->orderBy('id', 'DESC')->paginate($perPage);

        return $result;
    }


}
