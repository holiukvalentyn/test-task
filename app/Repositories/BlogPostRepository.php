<?php

namespace App\Repositories;

use App\Models\BlogPost as Model;

class BlogPostRepository extends CoreRepositories
{
    /**
     * @return mixed|void
     */
    protected function GetModelClass()
    {
        return Model::class;
    }

    /**
     * @param $perPage
     *
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = [
            'id',
            'title',
            'slug',
            'description',
            'category_id',
//            'category_title'
        ];

        $result = $this->startConditions()
                        ->select($columns)
                        ->orderBy('id', 'DESC')
                        ->with(['category'])
                        ->paginate($perPage);

        return $result;
    }

    /**
     * @param $id
     *
     * @return \App\Models\BlogPost
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    public function getShow($id)
    {
        return $this->startConditions()->find($id);
    }

    public function getForcomboBox()
    {
        $columns = implode(', ', [
            'id', 'CONCAT (id, ". ", title) AS id_title '
        ]);

        $result = $this->startConditions()
            ->selectRaw($columns)
            ->toBase()
            ->get();

        return $result;
    }
}
