<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class CoreRepositories
{
    /**
     * @var Model
     */
    protected $model;

    /**
     *
     */
    public function __construct()
    {
        $this->model = app($this->GetModelClass());
    }

    /**
     * @return mixed
     */
    abstract protected function GetModelClass();

    /**
     * @return mixed
     */
    protected function startConditions()
    {
      return clone $this->model;
    }


}
